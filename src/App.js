import React from "react";
import Routes from "./routes";
import "./styles/global.css";

// arquivo principal da aplicação
export default function App() {
  return <Routes />;
}