import React from "react";
import Menu from "../Menu";
import Footer from "../Footer";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

export default function Container({ children }) {
  return (
    <div className="container">
      <Menu />

      <div className="content">
        {children}
      </div>

      <ToastContainer position="bottom-right" />
      
      <Footer />
    </div>
  );
}