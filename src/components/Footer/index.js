import React from "react";
import { FaFacebook, FaInstagram, FaTwitter, FaGooglePlay } from "react-icons/fa";
import "./style.css";

export default function Footer() {
  return (
    <div className="footer">
      <p>Copyright 2021 Área Local - Todos os Direitos Reservados.</p>

      <ul>
        <li>
          <a href="https://www.facebook.com/aplicativodestsustentavel" target="blank">
            <FaFacebook size={22} color="#007b36" />
          </a>
        </li>
        <li>
          <a href="https://www.instagram.com/destsustentavel/" target="blank">
            <FaInstagram size={22} color="#007b36" />
          </a>
        </li>
        <li>
          <a href="https://twitter.com/sustentaveldest" target="blank">
            <FaTwitter size={22} color="#007b36" />
          </a>
        </li>
        <li>
          <a href="https://play.google.com/store/apps/details?id=org.destinosustentavel.dsmovel&hl=pt_BR" target="blank">
            <FaGooglePlay size={22} color="#007b36" />
          </a>
        </li>
      </ul>
    </div>
  );
}