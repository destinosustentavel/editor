import React from "react";
import "./style.css";

export default function Modal({ children, open }) {
  return (
    <div
      className="modal-container"
      style={open ? { display: "flex" } : { display: "none" }}
    >
      <div className="modal-content">
        {children}
      </div>
    </div>
  );
}