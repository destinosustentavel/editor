import React, { useRef } from "react";
import logo from "../../images/logo.svg";
import "./style.css";

export default function Menu() {
  const menuToggleRef = useRef(null);
  const menuToggleSectionRef = useRef(null);

  // alternando para menu hamburguer
  function toggleMenu() {
    menuToggleRef.current.classList.toggle("on");
    menuToggleSectionRef.current.classList.toggle("on");
  }

  return (
    <div className="menu">
      <a href="/">
        <img src={logo} alt="Destino Sustentável"/>
      </a>

      <div ref={menuToggleSectionRef} className="menu-toggle-section">
        <nav className="menu-toogle-nav">
          <ul className="list-menu-toggle">
            <li onClick={toggleMenu}><a href="/equipe">EQUIPE</a></li>
            <li onClick={toggleMenu}><a href="/premios">PRÊMIOS E TÍTULOS</a></li>
            <li onClick={toggleMenu}><a href="/registro">REGISTRO</a></li>
          </ul>
        </nav>
      </div>

      <nav className="list-menu">
        <ul>
          <li><a href="/equipe">EQUIPE</a></li>
          <li><a href="/premios">PRÊMIOS E TÍTULOS</a></li>
          <li><a href="/registro">REGISTRO</a></li>
        </ul>
      </nav>

      <div ref={menuToggleRef} className="menu-toggle" onClick={toggleMenu}>
        <span className="one"></span>
        <span className="two"></span>
        <span className="three"></span>
      </div>
    </div>
  );
}