import React from "react";
import { BrowserRouter, Route } from "react-router-dom";

// importando páginas
import Login from "./pages/Login";
import Home from "./pages/Home";
import Team from "./pages/Team";
import Awards from "./pages/Awards";
import Register from "./pages/Register";

// definindo rotas da aplicação
export default function Routes() {
  return (
    <BrowserRouter>
      <Route path="/login" component={Login} />
      <Route path="/" exact component={Home} />
      <Route path="/equipe" component={Team} />
      <Route path="/premios" component={Awards} />
      <Route path="/registro" component={Register} />
    </BrowserRouter>
  );
}