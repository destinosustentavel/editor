import React, { useEffect } from "react";
import { useHistory } from "react-router-dom";
import Container from "../../components/Container";

export default function Home() {
  const history = useHistory();
  const titleStyle = { color: "#fff" }
  
  // verificando se o usuário está autenticado
  useEffect(() => {
    const hash = sessionStorage.getItem("hash");
    if (!hash) history.push("/login");
  }, []);

  return (
    <Container>
      <h1 style={titleStyle}>Bem vindo ao Editor do Destino Sustentável!</h1>
    </Container>
  );
}