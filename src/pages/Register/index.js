import React, { useState, useEffect, useRef } from "react";
import { useHistory } from "react-router-dom";
import { toast } from "react-toastify";
import generator from "generate-password";
import Container from "../../components/Container";
import "./style.css";

import api from "../../services/api";

export default function Register() {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const history = useHistory();

  const inputRef = useRef(null);

  // validando formulário e registrando novo usuário
  async function submitForm() {
    if (!username || !password) {
      toast.error("Formulário incompleto.");
    } else {
      try {
        await api.post("/user", { username, password });
        toast.success("Novo usuário registrado com sucesso.");
        resetForm();
      } catch (err) {
        toast.error("Erro ao registrar o usuário.");
        console.error("Erro ao registrar o usuário: ", err);
      }
    }
  }

  // limpando campos do formulário
  function resetForm() {
    setUsername("");
    setPassword("");
  }

  // mudando visibilidade da senha
  function togglePassword() {
    const inputVisibility = inputRef.current.type;
    inputRef.current.type = inputVisibility === "text" ? "password" : "text";
  }

  // gerando senha de 8 dígitos
  function generatePassword() {
    const passwordGenerated = generator.generate({
      length: 8,
      numbers: true,
    });

    setPassword(passwordGenerated);
  }

  // verificando se o usuário está autenticado
  useEffect(() => {
    const hash = sessionStorage.getItem("hash");
    if (!hash) history.push("/login");
  }, []);

  return (
    <Container>
      <div className="login-container">
        <div className="form-login">
          <input
            className="form-input"
            type="text"
            value={username}
            placeholder="Nome de usuário"
            onChange={e => setUsername(e.target.value)}
          />

          <input
            ref={inputRef}
            className="form-input"
            type="password"
            value={password}
            placeholder="Senha"
            onChange={e => setPassword(e.target.value)}
          />

          <div className="password-container">
            <button className="password-button" onClick={togglePassword}>Ver senha</button>
            <button className="password-button" onClick={generatePassword}>Gerar senha</button>
          </div>

          <button className="submit-button" onClick={submitForm}>Registrar</button>
        </div>
      </div>
    </Container>
  );
}