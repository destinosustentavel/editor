import React, { useState, useEffect } from "react";
import Container from "../../components/Container";
import Modal from "../../components/Modal";
import { useHistory } from "react-router-dom";
import { toast } from "react-toastify";
import "./style.css";

import api from "../../services/api";

export default function Team() {
  const [name, setName] = useState("");
  const [job, setJob] = useState("");
  const [category, setCategory] = useState("");
  const [thumbnail, setThumbnail] = useState("");
  const [id, setId] = useState(null);
  const [teamData, setTeamData] = useState([]);
  const [modalOpen, setModalOpen] = useState(false);
  const history = useHistory();

  // buscando dados dos membros na api
  async function getTeamData() {
    const response = await api.get("/team");
    if (!response.data.error) setTeamData(response.data);
  }

  // validando formulário e enviando dados para a api
  async function submitForm() {
    if (!name || !job || !category || !thumbnail) {
      toast.error("Formulário incompleto.");
      return;
    }

    const data = { id, name, job, category, thumbnail };

    // caso tenha id o membro é alterado
    // se não tiver id é criado um novo membro
    if (id) {
      try {
        await api.put("/team", data);
        toast.success("Membro alterado com sucesso.");
        setId(null);
        resetForm();
      } catch (err) {
        toast.error("Erro ao alterar o membro.");
        console.error("Erro ao alterar o membro: ", err);
      }
    } else {
      try {
        await api.post("/team", data);
        toast.success("Membro adicionado com sucesso.");
        setId(null);
        resetForm();
      } catch (err) {
        toast.error("Erro ao registrar o membro.");
        console.error("Erro ao registrar o membro: ", err);
      }
    }
  }

  // colocando dados nos inputs para alteração
  function alterItem(team) {
    setName(team.name);
    setJob(team.job);
    setCategory(team.category);
    setThumbnail(team.thumbnail);
    setId(team.id);

    setModalOpen(false);
  }

  // removendo item da api usando seu id
  async function removeItem(id) {
    try {
      await api.delete(`/team/${id}`);
      toast.success("Membro excluído com sucesso.");
      setModalOpen(false);
    } catch (err) {
      toast.error("Erro ao remover o membro.");
      console.error("Erro ao remover o membro: ", err);
    }
  }

  // limpando campos do formulário
  function resetForm() {
    setName("");
    setJob("");
    setCategory("");
    setThumbnail("");
  }

  // abrindo modal
  function openModal() {
    getTeamData();
    setModalOpen(true);
  }

  // buscando dados da api ao carregar a página
  useEffect(() => {
    getTeamData();
  }, []);

  // verificando se o usuário está autenticado
  useEffect(() => {
    const hash = sessionStorage.getItem("hash");
    if (!hash) history.push("/login");
  }, []);

  return (
    <Container>
      <Modal open={modalOpen}>
        <div className="data-list">
          <span className="close-button" onClick={() => setModalOpen(false)}>x</span>
          {teamData.map(member => (
            <div className="list-item" key={member.id}>
              <span className="item-name">{member.name}</span>
              <div className="item-buttons">
                <button onClick={() => alterItem(member)}>Alterar</button>
                <button onClick={() => removeItem(member.id)}>Excluir</button>
              </div>
            </div>
          ))}
        </div>
      </Modal>

      <div className="form-team">
        <input
          className="form-input"
          type="text"
          value={thumbnail}
          placeholder="URL da Imagem"
          onChange={e => setThumbnail(e.target.value)}
        />

        <input
          className="form-input"
          type="text"
          value={name}
          placeholder="Nome"
          onChange={e => setName(e.target.value)}
        />

        <input
          className="form-input"
          type="text"
          value={job}
          placeholder="Função"
          onChange={e => setJob(e.target.value)}
        />

        <select
          className="form-select"
          value={category}
          onChange={e => setCategory(e.target.value)}
        >
          <option value="">Selecione uma categoria</option>
          <option value="equipe">Equipe</option>
          <option value="voluntario">Voluntário</option>
        </select>

        <div className="button-container">
          <button className="submit-button" onClick={submitForm}>
            {id ? "Alterar": "Adicionar"}
          </button>

          {!id && (
            <button className="submit-button" onClick={openModal}>Alterar / Excluir</button>
          )}
        </div>
      </div>
    </Container>
  );
}