import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import Container from "../../components/Container";
import Modal from "../../components/Modal";
import { toast } from "react-toastify";
import "./style.css";

import api from "../../services/api";

export default function Awards() {
  const [title, setTitle] = useState("");
  const [date, setDate] = useState("");
  const [resume, setResume] = useState("");
  const [thumbnail, setThumbnail] = useState("");
  const [id, setId] = useState(null);
  const [awardsData, setAwardsData] = useState([]);
  const [modalOpen, setModalOpen] = useState(false);
  const history = useHistory();

  // buscando dados dos prêmios na api
  async function getAwardsData() {
    const response = await api.get("/awards");
    if (!response.data.error) setAwardsData(response.data);
  }

  // validando formulário e enviando os dados para a api
  async function submitForm() {
    if (!title || !date || !resume || !thumbnail) {
      toast.error("Formulário incompleto.");
      return;
    }

    const data = { id, title, date, resume, thumbnail };

    // caso tenha id o prêmio é alterado
    // se não tiver id é criado um novo prêmio
    if (id) {
      try {
        await api.put("/awards", data);
        toast.success("Prêmio alterado com sucesso.");
        setId(null);
        resetForm();
      } catch (err) {
        toast.error("Erro ao alterar o prêmio.");
        console.error("Erro ao alterar o prêmio: ", err);
      }
    } else {
      try {
        await api.post("/awards", data);
        toast.success("Prêmio adicionado com sucesso.");
        resetForm();
      } catch (err) {
        toast.error("Erro ao registrar o prêmio.");
        console.error("Erro ao registrar o prêmio: ", err);
      }
    }
  }

  // colocando dados nos inputs para alteração
  function alterItem(award) {
    setTitle(award.title);
    setDate(award.date);
    setResume(award.resume);
    setThumbnail(award.thumbnail);
    setId(award.id);

    setModalOpen(false);
  }

  // removendo item da api usando seu id
  async function removeItem(id) {
    try {
      await api.delete(`/awards/${id}`);
      toast.success("Prêmio excluído com sucesso.");
      setModalOpen(false);
    } catch (err) {
      toast.error("Erro ao remover o prêmio.");
      console.error("Erro ao remover o prêmio: ", err);
    }
  }

  // limpando campos do formulário
  function resetForm() {
    setTitle("");
    setDate("");
    setResume("");
    setThumbnail("");
  }

  // abrindo modal
  function openModal() {
    getAwardsData();
    setModalOpen(true);
  }

  // buscando dados da api ao carregar a página
  useEffect(() => {
    getAwardsData();
  }, []);

  // verificando se o usuário está autenticado
  useEffect(() => {
    const hash = sessionStorage.getItem("hash");
    if (!hash) history.push("/login");
  }, []);

  return (
    <Container>
      <Modal open={modalOpen}>
        <div className="data-list">
          <span className="close-button" onClick={() => setModalOpen(false)}>x</span>
          {awardsData.map(award => (
            <div className="list-item" key={award.id}>
              <span className="item-name">{award.title}</span>
              <div className="item-buttons">
                <button onClick={() => alterItem(award)}>Alterar</button>
                <button onClick={() => removeItem(award.id)}>Excluir</button>
              </div>
            </div>
          ))}
        </div>
      </Modal>

      <div className="form-awards">
        <input
          className="form-input"
          type="text"
          value={thumbnail}
          placeholder="URL da Imagem"
          onChange={e => setThumbnail(e.target.value)}
        />

        <input
          className="form-input"
          type="text"
          value={title}
          placeholder="Título"
          onChange={e => setTitle(e.target.value)}
        />

        <input
          className="form-input"
          type="text"
          value={date}
          placeholder="Data"
          onChange={e => setDate(e.target.value)}
        />

        <textarea
          className="form-resume"
          rows="7"
          value={resume}
          placeholder="Resumo"
          onChange={e => setResume(e.target.value)}
        />

        <div className="button-container">
          <button className="submit-button" onClick={submitForm}>
            {id ? "Alterar": "Adicionar"}
          </button>

          {!id && (
            <button className="submit-button" onClick={openModal}>Alterar / Excluir</button>
          )}
        </div>
      </div>
    </Container>
  );
}