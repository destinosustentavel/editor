import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import "./style.css";

import api from "../../services/api";

export default function Login() {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const history = useHistory();

  // validando formulário e autenticando usuário
  async function submitForm() {
    if (!username || !password) {
      toast.error("Formulário incompleto.");
    } else {
      try {
        const response = await api.post(`user/${username}`, { password });
        const { authenticated, hash } = response.data;

        if (authenticated) {
          sessionStorage.setItem("hash", hash);
          history.push("/");
        } else {
          toast.error("Usuário ou senha incorretos.");
        }
      } catch (err) {
        toast.error("Erro ao fazer login.");
        console.error("Erro ao fazer login: ", err);
      }
      
    }
  }

  // verificando se o usuário está autenticado
  useEffect(() => {
    const hash = sessionStorage.getItem("hash");
    if (hash) history.push("/");
  }, []);

  return (
    <div className="login-container">
      <div className="form-login">
        <input
          className="form-input"
          type="text"
          value={username}
          placeholder="Nome de usuário"
          onChange={e => setUsername(e.target.value)}
        />

        <input
          className="form-input"
          type="password"
          value={password}
          placeholder="Senha"
          onChange={e => setPassword(e.target.value)}
        />

        <button className="submit-button" onClick={submitForm}>Entrar</button>
      </div>

      <ToastContainer position="bottom-right" />
    </div>
  );
}