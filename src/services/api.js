import axios from "axios";

// definindo url base da api
const api = axios.create({
  baseURL: "http://localhost:5000"
});

export default api;